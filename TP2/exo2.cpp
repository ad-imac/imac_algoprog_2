#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
    Array& sorted=w->newArray(toSort.size());
    sorted[0] = toSort[0];
    int n, m;
    for (n = 1; n < (int)toSort.size(); n++) {
        for (m = 0; m < n; m++) {
            if (sorted[m] > toSort[n]) {
                break;
            }
        }
        sorted.insert(m,toSort[n]);
    }
    toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
