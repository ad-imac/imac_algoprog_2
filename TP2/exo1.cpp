#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
    int min_index = 0;
    for (int i = 0; i < toSort.size() - 1; i++) {
        min_index = i;
        for (int j = i; j < toSort.size(); j++) {
            if (toSort[j] < toSort[min_index]) {
                min_index = j;
            }
        }
        toSort.swap(min_index,i);
    }
}

int main(int argc, char *argv[]) {
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
